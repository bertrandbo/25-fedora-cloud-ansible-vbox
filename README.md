# Ansible and VBox friendly Fedora Cloud box

This repository provides playbooks to add Ansible and Virtual Box Guest Additions to the [official Fedora Cloud box](https://app.vagrantup.com/fedora/boxes/25-cloud-base).

Ansible and its minimal requirements are installed from offical Fedora repositories, based on [this article](https://fedoramagazine.org/getting-ansible-working-fedora-23/) and [this discussion](https://lists.fedoraproject.org/pipermail/devel/2015-October/215740.html).

Guest Additions are installed from [the free repository of RPM Fusion](https://rpmfusion.org/). Unfortunately, it fetches a lot of X.Org dependencies.
